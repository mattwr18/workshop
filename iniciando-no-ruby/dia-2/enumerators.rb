# enumerator.rb
enumerator = %w[RubyFloripa WorkshopDia2 LiveCoding].each
p enumerator

enumerator.each_with_object('name') do |item, obj|
  p "#{obj}: #{item}"
end

p [%w[fun with coding].map.with_index { |w, i| "#{i}:#{w}" }]
# external iteration
p e = [1, 2, 3].each
p e.next
p e.next
p e.next
# internal iteration
enumerator = %w[RubyFloripa LiveCoding WorkshopDia2 PairProgramming]
p enumerator
enumerator.map { |i| p i.downcase }
p enumerator
enumerator.select { |i| p i.length > 10 ? i.upcase : 'Too short.' }
p enumerator
enumerator.inject do |memo, word|
  p memo.length > word.length ? memo : word
end
p enumerator
