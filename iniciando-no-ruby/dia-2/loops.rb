# loops.rb
class Company < Struct.new(:name)
  def request_time_off(num)
    for number_of_employees in 1..num do
      p "Employees to request time off: #{number_of_employees}"
    end
  end
end

new_company = Company.new('RubyFloripa')
new_company.request_time_off(4)

3.times do |something|
  p "Something really cool #{something + 1} times."
end

1.upto(4) { |i| p "I'm counting: #{i}" }

x = 4
p "Runs until conditions met: #{x -= 1}" while x > 0

array = ['bananas', 'oranges', 'apples', 'passion fruit']
array.each { |fruit| p "Would you like some #{fruit}" }

companies = [{ name: 'RubyFloripa', associates: 20 },
             { name: 'GitLab', associates: 1000 },
             { name: 'StartUp', associates: 5 }]
companies.each do |hash|
  hash.each do |k, v|
    p (v.is_a? Integer) ? "There are #{v} #{k}" : "#{k}: #{v}"
  end
end
