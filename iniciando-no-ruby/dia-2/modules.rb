# modules.rb
module HealthProperties
  def vitamin_b?(food)
    p (food.class == Legume) ? true : false
  end

  def vitamin_c?(food)
    p (food.class == CitricFruit) ? true : false
  end
end

class Legume
  include HealthProperties
  def order(quantity)
    price = 5
    @order = (quantity.to_i * price)
    p @order
  end
end

class CitricFruit
  include HealthProperties
  def more_fruit
    fruit = 0
    more_fruit = 1
    fruit += more_fruit
    p fruit
  end
end
legume = Legume.new
legume.order(2)
legume.vitamin_b?(legume)
legume.vitamin_c?(legume)
citric_fruit = CitricFruit.new
citric_fruit.more_fruit
citric_fruit.vitamin_b?(citric_fruit)
citric_fruit.vitamin_c?(citric_fruit)
