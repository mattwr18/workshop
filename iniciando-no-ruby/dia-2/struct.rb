# struct.rb
Company = Struct.new(:name, :size) do
  def number_of_employees
    size
  end
end

company1 = Company.new('RubyFloripa', 20)
p company1.name # "RubyFloripa"
p company1.size # 20
p company1.number_of_employees # 20

class Company
  attr_accessor :name, :size

  def initialize(name, size)
    @name = name
    @size = size
  end

  def number_of_employees
    size
  end
end

company1 = Company.new('RubyFloripa', 20)
p company1.name # "RubyFloripa"
p company1.size # 20
p company1.number_of_employees # 20

class Company
  Address = Struct.new(:street_1, :street_2,
                       :city, :state, :country,
                       :zip_code)

  attr_accessor :name, :address

  def initialize(name, opts)
    @name = name
    @address = Address.new(opts[:street_1], opts[:street_2],
                           opts[:city], opts[:state], opts[:country],
                           opts[:zip_code])
  end
end

new_company = Company.new('RubyFloripa',
                          street_1: '123 Road',
                          city: 'Long Beach',
                          state: 'CA',
                          country: 'USA',
                          zip_code: '90712')
p new_company.address
