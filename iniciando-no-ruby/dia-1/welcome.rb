# welcome.rb
event = {
  name: 'RubyFloripa',
  type: 'Workshop',
  attendees: 36,
  date: Time.new(2018, 05, 19, 8, 15)
}

puts "Bem vindo ao #{event[:name]}"
puts "-> #{event[:type]}"
puts "-> #{event[:attendees]} participantes"
puts "-> #{event[:date].strftime('%d/%m/%Y %H:%M:%S')}"
