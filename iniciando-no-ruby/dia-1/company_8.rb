# company_8.rb
class Company
  Address = Struct.new(:street_1, :street_2,
                       :city, :state, :country,
                       :zip_code)

  attr_accessor :name, :address

  def initialize(name, opts)
    @name = name
    @address = Address.new(opts[:street_1], opts[:street_2],
                           opts[:city], opts[:state], opts[:country],
                           opts[:zip_code])
  end
end

new_company = Company.new('RubyFloripa',
                          street_1: '123 Road',
                          city: 'Long Beach',
                          state: 'CA',
                          country: 'USA',
                          zip_code: '90712')
p new_company.address
